import logging
import threading
import os

import diskcache
from grid5000 import Grid5000
import ring


_api_lock = threading.Lock()
# Keep track of the api client
_api_client = None

storage = diskcache.Cache('cachedir')

def get_api_client():
    """Gets the reference to the API cient (singleton)."""
    with _api_lock:
        global _api_client
        if not _api_client:
            conf_file = os.path.join(os.environ.get("HOME"),
                                    ".python-grid5000.yaml")
            _api_client = Grid5000.from_yaml(conf_file)

        return _api_client


@ring.disk(storage)
def get_sites_obj():
    """Get all the sites."""
    gk = get_api_client()
    return gk.sites.list()


@ring.disk(storage)
def get_all_clusters_obj():
    """Get all the clusters."""
    sites = get_sites_obj()
    clusters = []
    for site in sites:
        # should we cache the list aswell ?
        clusters.extend(site.clusters.list())
    return clusters


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    clusters = get_all_clusters_obj()
    print(clusters)
    print("Known key in the cache")
    print(get_all_clusters_obj.get())
    print("Calling again the function is now faster")
    clusters = get_all_clusters_obj()
    print(clusters)

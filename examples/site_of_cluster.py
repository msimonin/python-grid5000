import logging
import os

from grid5000 import Grid5000


logging.basicConfig(level=logging.DEBUG)

clusters = ["dahu", "parasilo", "chetemi"]

conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)

sites = gk.sites.list()
matches = []
for site in sites:
    candidates = site.clusters.list()
    matching = [c.uid for c in candidates if c.uid in clusters]
    if len(matching) == 1:
        matches.append((site, matching[0]))
        clusters.remove(matching[0])
print("We found the following matches %s" % matches)

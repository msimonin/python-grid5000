import logging
import os
import time

from grid5000 import Grid5000


logging.basicConfig(level=logging.DEBUG)

conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)

# This is equivalent to gk.sites.get("rennes")
site = gk.sites["rennes"]

job = site.jobs.create({"name": "pyg5k",
                        "command": "sleep 3600",
                        "types": ["deploy"]})

while job.state != "running":
    job.refresh()
    print("Waiting the job [%s] to be running" % job.uid)
    time.sleep(10)

print("Assigned nodes : %s" % job.assigned_nodes)

deployment = site.deployments.create({"nodes": job.assigned_nodes,
                                      "environment": "debian9-x64-min"})
# To get SSH access to your nodes you can pass your public key
#
# from pathlib import Path
#
# key_path = Path.home().joinpath(".ssh", "id_rsa.pub")
#
#
# deployment = site.deployments.create({"nodes": job.assigned_nodes,
#                                       "environment": "debian9-x64-min"
#                                       "key": key_path.read_text()})

while deployment.status != "terminated":
    deployment.refresh()
    print("Waiting for the deployment [%s] to be finished" % deployment.uid)
    time.sleep(10)

print(deployment.result)

import logging
import os
import time

from grid5000 import Grid5000


logging.basicConfig(level=logging.DEBUG)

conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)

# This is equivalent to gk.sites.get("rennes")
site = gk.sites["rennes"]

job = site.jobs.create({"name": "pyg5k",
                        "command": "sleep 3600"})

while job.state != "running":
    job.refresh()
    print("Waiting for the job [%s] to be running" % job.uid)
    time.sleep(10)

print(job)
print("Assigned nodes : %s" % job.assigned_nodes)

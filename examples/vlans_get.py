import logging
import os

from grid5000 import Grid5000


logging.basicConfig(level=logging.DEBUG)

conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)

site = gk.sites["rennes"]

# Get all vlans
vlans = site.vlans.list()
print(vlans)

# Get on specific
vlan = site.vlans.get("4")
print(vlan)

vlan = site.vlans["4"]
print(vlan)

# Get vlan of some nodes
print(site.vlansnodes.submit(["paravance-1.rennes.grid5000.fr", "paravance-2.rennes.grid5000.fr"]))


# Get nodes in vlan
print(site.vlans["4"].nodes.list())

import logging
import os

from grid5000 import Grid5000


logging.basicConfig(level=logging.DEBUG)

conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)

rennes = gk.sites["rennes"]
site_statuses = rennes.status.list()
print(site_statuses)

cluster = rennes.clusters["paravance"]
cluster_statuses = cluster.status.list()

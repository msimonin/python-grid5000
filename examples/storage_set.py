from netaddr import IPNetwork
import logging
import os
import time

from grid5000 import Grid5000


logging.basicConfig(level=logging.DEBUG)

conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)
site = gk.sites["rennes"]

job = site.jobs.create({"name": "pyg5k",
                        "command": "sleep 3600",
                        "resources": "slash_22=1+nodes=1"})

while job.state != "running":
    job.refresh()
    print("Waiting the job [%s] to be running" % job.uid)
    time.sleep(5)

subnet = job.resources_by_type['subnets'][0]
ip_network = [str(ip) for ip in IPNetwork(subnet)]

# create acces for all ips in the subnet
access = site.storage["home"].access["msimonin"].rules.create({"ipv4": ip_network,
                                                               "termination": {
                                                                  "job": job.uid,
                                                                  "site": site.uid}})

# listing the accesses
print(gk.sites["rennes"].storage["home"].access["msimonin"].rules.list())

# 1.2.2

- Get cleaner (client) endpoinf for the storage API
# 1.2.1

- StorageHomeUser now uses id as __attr_id (fix an issue in `__repr__`)

# 1.2.0

- Improve use experience with SSL certificates
  + We now load the system CA bundle if it exists (fix errors when running inside G5K)
- Misc:
  + Grid5000 shell colors
  + RST doc regenerated

# 1.1.3

- Fix metrics endpoints

# 1.1.2

- Extract a API method to authenticate from python
# 1.1.1

- Introduce `grid5000-auth` to facilitate the creation of credential file

# 1.1.0

-  Add programmable firewall interface

# 1.0.3

- Introduce offline client (allow to browse the ref api offline)

# 0.3.0

- Two object are equals if they share the same id (`get_id`)

# 0.2.3

- Allow for a default ssl user in the client
- fix a regression with the shell
- some minor doc fixes

# 0.2.2

- Support for timeseries of various metrics
- pep8 fix
- fix CI jobs to check the pep8

# 0.2.1

- catch up changelog

# 0.2.0

- Doc: Enable SSL verification inside G5K

# 0.1.4

- Doc: Enable SSL verification inside G5K

# 0.1.3

- Make the configuration file optionnal (e.g anonymous connection on G5K frontend)

# 0.1.2

- (Ok I've lost the changelog at this point)

# 0.0.19

- Add a section about caching

# 0.0.18

- Make sure _module lands in __dict__ when deserializing

# 0.0.17

- Catch up changelog

# 0.0.16

- Document how to pass the ssh key

# 0.0.15

- Force user-agent to python-grid5000

# 0.0.14

- Use json dumps as string representation
- [cli] Check if the configuration file exists
- [base] Relax constructor parameters

# 0.0.13

- Add BracketMixin for deployments
- Expose server resource

# 0.0.12

- Update pyaml dependency to latest 5.1

# 0.0.11

- Job object: decorate `__repr__` with (user, site, uid, state)

# 0.0.10

- Add a motd

# 0.0.9

- Fix embedded shell dependency

# 0.0.8

- Add an embedded shell

# 0.0.7

- Add status endpoints

# 0.0.6

- Enable versions retrieval on existing objects

# 0.0.5

- Enable bracket for jobs

# 0.0.4

- Decrease the number of retries
- Implement DELETE for jobs
- Update the example list

# 0.0.3

- ci testing

# 0.0.2

- small iterations

# 0.0.1

- initial release
